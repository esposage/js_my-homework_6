/*Теоретичні питання
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
2. Які засоби оголошення функцій ви знаєте?
3. Що таке hoisting, як він працює для змінних та функцій?

Відповіді
1. Екранування - це спосіб переведення спеціальних символів типу (, ., ?, {} і т.д. на звичайні для їх подальшого використання.
Екранують символи за допомогою слешу "\". 
Для екранування символів в регулярних виразах використовується подвійний слеш "\\".
2. Засоби оголошення функції: 
- Function Declaration - функція, яка об"явлена в основному потоці коду. Приклад: 
function name(arguments) {
console.log(arguments);
}
- Function Expression - функція, яка оголошена у складі виразу. Наприклад, функія оголошена як змінна.
let name = function nameFunction(a, b) {
  return a + b;
}
3. hoisting - це процес підняття змінних і оголошення функцій вверх по своїй області видимості до того , як код буде виконаний. 
Це дозволяє викликати функції перед тим, як вони були оголошені. Змінні в результаті hoisting переміщуються вверх по своїй області видимості, 
незалежно від того глобальна це чи локальна область видимості. Але при цьому значення змінних залишаються на своєму місці в коді.
*/
let firstName = prompt ("Enter your name").slice(0, 1);
let lastName = prompt ("Enter your last name").toLowerCase();
let birthDate = prompt ("Enter your date of birth in dd.mm.yyyy format");
let day = parseInt(birthDate.substring(0,2));
let month = parseInt(birthDate.substring(3,5));
let year = parseInt(birthDate.substring(6,10));
let birthday = new Date(year, month - 1, day);
let today = new Date();
function createNewUser(firstName, lastName, birthDate) {
  return {
    firstName,
    lastName,
    birthDate,
    getLogin() {
      console.log(`${this.firstName}${this.lastName}`);
    },
    getAge() {
      let age = today.getFullYear() - birthday.getFullYear();
      let m = today.getMonth() - birthday.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
      console.log(age);
    },
    getPasswod() {
  console.log(`${this.firstName}${this.lastName}${year}`);
}
  }
}
const newUser = createNewUser(firstName, lastName, birthDate);
newUser.getLogin();
newUser.getAge();
newUser.getPasswod();
